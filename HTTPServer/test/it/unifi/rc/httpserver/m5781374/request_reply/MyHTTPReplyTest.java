package it.unifi.rc.httpserver.m5781374.request_reply;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.concurrent.ConcurrentHashMap;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import it.unifi.rc.httpserver.m5781374.request_reply.MyHTTPReply;

class MyHTTPReplyTest {

	static private ConcurrentHashMap<String, String> map;
	static private MyHTTPReply reply;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		map = new ConcurrentHashMap<String, String>();
		map.put("Server", "Apache/2.0.52 (CentOS)");
		reply = new MyHTTPReply("HTTP/1.1", "200", "OK", "data", map);
	}

	@Test
	void testGetVersion() {
		assertEquals("HTTP/1.1", reply.getVersion());
		assertNotEquals("HTTP/1.0", reply.getVersion());
	}

	@Test
	void testGetStatusCode() {
		assertEquals("200", reply.getStatusCode());
		assertNotEquals("404", reply.getStatusCode());
	}

	@Test
	void testGetStatusMessage() {
		assertEquals("OK", reply.getStatusMessage());
		assertNotEquals("Not Found", reply.getStatusMessage());
	}

	@Test
	void testGetData() {
		assertEquals("data", reply.getData());
		assertNotEquals("not_data", reply.getData());
	}

	@Test
	void testGetParameters() {
		assertEquals(map, reply.getParameters());
		ConcurrentHashMap<String, String> notMyMap = new ConcurrentHashMap<String, String>();
		notMyMap.put("Server", "Apache/2.2.52 (Debian)");
		assertNotEquals(notMyMap, reply.getParameters());
	}

	@Test
	void testEquals() {
		assertEquals(true, reply.equals(new MyHTTPReply("HTTP/1.1", "200", "OK", "data", map)));
		assertNotEquals(true, reply.equals(new MyHTTPReply("HTTP/1.0", "201", "OK", "data", map)));
	}

}
