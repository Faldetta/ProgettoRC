package it.unifi.rc.httpserver.m5781374.request_reply;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.ConcurrentHashMap;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import it.unifi.rc.httpserver.m5781374.request_reply.MyHTTPRequest;

class MyHTTPRequestTest {

	static private ConcurrentHashMap<String, String> map;
	static private MyHTTPRequest request;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		map = new ConcurrentHashMap<String, String>();
		map.put("Host", "https://www.garr.it/it/");
		request = new MyHTTPRequest("HTTP/1.1", "GET", "/index.html", "body", map);
	}

	@Test
	void testGetVersion() {
		assertEquals("HTTP/1.1", request.getVersion());
		assertNotEquals("HTTP/1.0", request.getVersion());
	}

	@Test
	void testGetMethod() {
		assertEquals("GET", request.getMethod());
		assertNotEquals("POST", request.getMethod());

	}

	@Test
	void testGetPath() {
		assertEquals("/index.html", request.getPath());
		assertNotEquals("/not_index.html", request.getPath());
	}

	@Test
	void testGetEntityBody() {
		assertEquals("body", request.getEntityBody());
		assertNotEquals("not_body", request.getEntityBody());
	}

	@Test
	void testGetParameters() {
		assertEquals(map, request.getParameters());
		ConcurrentHashMap<String, String> notMyMap = new ConcurrentHashMap<String, String>();
		notMyMap.put("Host", "https://www.garr.it/en/");
		assertNotEquals(notMyMap, request.getParameters());
	}

	@Test
	void testEquals() {
		assertEquals(true, request.equals(new MyHTTPRequest("HTTP/1.1", "GET", "/index.html", "body", map)));
		assertNotEquals(true, request.equals(new MyHTTPRequest("HTTP/1.0", "POST", "/index.html", "body", map)));
	}

}
