package it.unifi.rc.httpserver.m5781374.stream;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileInputStream;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.jupiter.api.Test;

import it.unifi.rc.httpserver.HTTPReply;
import it.unifi.rc.httpserver.HTTPRequest;
import it.unifi.rc.httpserver.m5781374.request_reply.MyHTTPReply;
import it.unifi.rc.httpserver.m5781374.request_reply.MyHTTPRequest;
import it.unifi.rc.httpserver.m5781374.streams.MyHTTPInputStream;

class MyHTTPInputStreamTest {

	@Test
	void testReadHttpRequest() {
		try {
			MyHTTPInputStream inputStream = new MyHTTPInputStream(
					new FileInputStream("test/it/unifi/rc/httpserver/m5781374/SampleHTTPRequestIn.txt"));
			ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
			map.put("Host", "www-net.cs.unmass.edu");
			HTTPRequest request = inputStream.readHttpRequest();
			inputStream.close();
			assertEquals("HTTP/1.1", request.getVersion());
			assertEquals("GET", request.getMethod());
			assertEquals("/index.html", request.getPath());
			assertEquals("", request.getEntityBody());
			assertEquals(map, request.getParameters());
			assertEquals(new MyHTTPRequest("HTTP/1.1", "GET", "/index.html", "", map), request);
			assertNotEquals("HTTP/1.0", request.getVersion());
			assertNotEquals("POST", request.getMethod());
			assertNotEquals("/not_index.html", request.getPath());
			assertNotEquals(" ", request.getEntityBody());
			map.clear();
			map.put("Host", "www-net.cs.unmass.it");
			assertNotEquals(map, request.getParameters());
			assertNotEquals(new MyHTTPRequest("HTTP/1.0", "POST", "/not_index.html", " ", map), request);
			inputStream.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	void testReadHttpReply() {
		try {
			MyHTTPInputStream inputStream = new MyHTTPInputStream(
					new FileInputStream("test/it/unifi/rc/httpserver/m5781374/SampleHTTPReplyIn.txt"));
			ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
			map.put("Server", "Apache/2.0.52 (CentOS)");
			// HTTPReply expected = new MyHTTPReply("HTTP/1.1", "200", "OK", "", map);
			HTTPReply reply = inputStream.readHttpReply();
			inputStream.close();
			assertEquals("HTTP/1.1", reply.getVersion());
			assertEquals("200", reply.getStatusCode());
			assertEquals("OK", reply.getStatusMessage());
			assertEquals("", reply.getData());
			assertEquals(map, reply.getParameters());
			assertEquals(new MyHTTPReply("HTTP/1.1", "200", "OK", "", map), reply);
			assertNotEquals("HTTP/1.0", reply.getVersion());
			assertNotEquals("404", reply.getStatusCode());
			assertNotEquals("NOT_OK", reply.getStatusMessage());
			assertNotEquals(" ", reply.getData());
			map.clear();
			map.put("Server", "Apache/2.0.52 (Debian)");
			assertNotEquals(map, reply.getParameters());
			assertNotEquals(new MyHTTPReply("HTTP/1.0", "404", "NOT_OK", " ", map), reply);
			inputStream.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}