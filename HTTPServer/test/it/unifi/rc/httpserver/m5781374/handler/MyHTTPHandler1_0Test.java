package it.unifi.rc.httpserver.m5781374.handler;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import it.unifi.rc.httpserver.HTTPReply;
import it.unifi.rc.httpserver.HTTPRequest;
import it.unifi.rc.httpserver.m5781374.handler.MyHTTPHandler1_0;
import it.unifi.rc.httpserver.m5781374.request_reply.MyHTTPReply;
import it.unifi.rc.httpserver.m5781374.request_reply.MyHTTPRequest;

class MyHTTPHandler1_0Test {

	private HTTPRequest request;
	private HTTPReply expectedReply, givenReply;
	private static MyHTTPHandler1_0 handler, handler_noHost;

	@BeforeAll
	static void BeforeAll() {
		handler = new MyHTTPHandler1_0(new File("test/it/unifi/rc/httpserver/m5781374"), "https://www.garr.it/en/");
		handler_noHost = new MyHTTPHandler1_0(new File("test/it/unifi/rc/httpserver/m5781374"));

	}

	@Test
	void testHandleWrongVersion() {
		ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
		map.put("Host", "https://www.garr.it/en/");
		request = new MyHTTPRequest("HTTP/1.1", "GET", "/index.html", "", map);
		expectedReply = new MyHTTPReply("HTTP/1.0", "505", "HTTP Version Not Supported", "", map);
		givenReply = handler.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
		givenReply = handler_noHost.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
	}

	@Test
	void testHandleNotFound() {
		ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
		map.put("Host", "https://www.garr.it/en/");
		request = new MyHTTPRequest("HTTP/1.0", "GET", "/indexx.html", "", map);
		expectedReply = new MyHTTPReply("HTTP/1.0", "404", "Not Found", "", map);
		givenReply = handler.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
		givenReply = handler_noHost.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
	}

	@Test
	void testHandleGet() {
		ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
		map.put("Host", "https://www.garr.it/en/");
		request = new MyHTTPRequest("HTTP/1.0", "GET", "/SampleHTTPRequestIn.txt", "", map);
		expectedReply = new MyHTTPReply("HTTP/1.0", "200", "OK",
				"GET /index.html HTTP/1.1\r\nHost: www-net.cs.unmass.edu\r\n\r\n", map);
		givenReply = handler.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
		givenReply = handler_noHost.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
	}

	@Test
	void testHandlePost() {
		ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
		map.put("Host", "https://www.garr.it/en/");
		request = new MyHTTPRequest("HTTP/1.0", "POST", "/PostFile.txt", "post", map);
		expectedReply = new MyHTTPReply("HTTP/1.0", "200", "OK", "RESULT", map);
		givenReply = handler.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
		givenReply = handler_noHost.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
	}

	@Test
	void testHandleHead() {
		ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
		map.put("Host", "https://www.garr.it/en/");
		request = new MyHTTPRequest("HTTP/1.0", "HEAD", "/SampleHTTPRequestIn.txt", "", map);
		expectedReply = new MyHTTPReply("HTTP/1.0", "200", "OK", "", map);
		givenReply = handler.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
		givenReply = handler_noHost.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
	}

}
