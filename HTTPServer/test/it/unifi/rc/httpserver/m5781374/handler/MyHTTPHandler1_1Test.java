package it.unifi.rc.httpserver.m5781374.handler;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import it.unifi.rc.httpserver.HTTPReply;
import it.unifi.rc.httpserver.HTTPRequest;
import it.unifi.rc.httpserver.m5781374.handler.MyHTTPAbstractHandler;
import it.unifi.rc.httpserver.m5781374.handler.MyHTTPHandler1_1;
import it.unifi.rc.httpserver.m5781374.request_reply.MyHTTPReply;
import it.unifi.rc.httpserver.m5781374.request_reply.MyHTTPRequest;

class MyHTTPHandler1_1Test {

	private HTTPRequest request;
	private HTTPReply expectedReply, givenReply;
	private static MyHTTPAbstractHandler handler, handler_noHost;

	@BeforeAll
	static void BeforeAll() {
		handler = new MyHTTPHandler1_1(new File("test/it/unifi/rc/httpserver/m5781374"),
				"https://www.garr.it/en/");
		handler_noHost = new MyHTTPHandler1_1(new File("test/it/unifi/rc/httpserver/m5781374"));

	}

	@Test
	void testHandleWrongVersion() {
		ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
		map.put("Host", "https://www.garr.it/en/");
		request = new MyHTTPRequest("HTTP/1.0", "GET", "/index.html", "", map);
		expectedReply = new MyHTTPReply("HTTP/1.0", "200", "OK", "", map);
		givenReply = handler.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
		givenReply = handler_noHost.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
	}

	@Test
	void testHandleNotFound() {
		ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
		map.put("Host", "https://www.garr.it/en/");
		request = new MyHTTPRequest("HTTP/1.1", "GET", "/indexx.html", "", map);
		expectedReply = new MyHTTPReply("HTTP/1.1", "404", "Not Found", "", map);
		givenReply = handler.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
		givenReply = handler_noHost.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
	}

	@Test
	void testHandleGet() {
		ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
		map.put("Host", "https://www.garr.it/en/");
		request = new MyHTTPRequest("HTTP/1.1", "GET", "/SampleHTTPRequestIn.txt", "", map);
		expectedReply = new MyHTTPReply("HTTP/1.1", "200", "OK",
				"GET /index.html HTTP/1.1\r\nHost: www-net.cs.unmass.edu\r\n\r\n", map);
		givenReply = handler.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
		givenReply = handler_noHost.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
	}

	@Test
	void testHandlePost() {
		ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
		map.put("Host", "https://www.garr.it/en/");
		request = new MyHTTPRequest("HTTP/1.1", "POST", "/PostFile.txt", "post", map);
		expectedReply = new MyHTTPReply("HTTP/1.1", "200", "OK", "RESULT", map);
		givenReply = handler.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
		givenReply = handler_noHost.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
	}

	@Test
	void testHandleHead() {
		ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
		map.put("Host", "https://www.garr.it/en/");
		request = new MyHTTPRequest("HTTP/1.1", "HEAD", "/SampleHTTPRequestIn.txt", "", map);
		expectedReply = new MyHTTPReply("HTTP/1.1", "200", "OK", "", map);
		givenReply = handler.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
		givenReply = handler_noHost.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
	}

	@Test
	void testHandlePut() {
		ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
		map.put("Host", "https://www.garr.it/en/");
		request = new MyHTTPRequest("HTTP/1.1", "PUT", "/PutFile.txt", "put", map);
		expectedReply = new MyHTTPReply("HTTP/1.1", "204", "No Content", "", map);
		givenReply = handler.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
		givenReply = handler_noHost.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
	}

	@Test
	void testHandleDelete() {
		ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
		map.put("Host", "https://www.garr.it/en/");
		request = new MyHTTPRequest("HTTP/1.1", "DELETE", "/toDelete.txt", "", map);
		expectedReply = new MyHTTPReply("HTTP/1.1", "204", "No Content", "", map);
		File toDelete = new File("test/it/unifi/rc/httpserver/m5781374/toDelete.txt");
		try {
			toDelete.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		givenReply = handler.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
		try {
			toDelete.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		givenReply = handler_noHost.handle(request);
		assertEquals(expectedReply.getVersion(), givenReply.getVersion());
		assertEquals(expectedReply.getStatusCode(), givenReply.getStatusCode());
		assertEquals(expectedReply.getStatusMessage(), givenReply.getStatusMessage());
		assertEquals(expectedReply.getData(), givenReply.getData());
	}

}
