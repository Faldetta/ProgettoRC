package it.unifi.rc.httpserver.m5781374.handler;

import java.io.File;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import it.unifi.rc.httpserver.HTTPHandler;
import it.unifi.rc.httpserver.HTTPReply;
import it.unifi.rc.httpserver.HTTPRequest;
import it.unifi.rc.httpserver.m5781374.request_reply.MyHTTPReply;

/**
 * Class defining a 1.0 {@link HTTPHandler}
 * 
 * @author bow
 *
 */
public class MyHTTPHandler1_0 extends MyHTTPAbstractHandler implements HTTPHandler {

	private String version;

	/**
	 * Constructor using field
	 * 
	 * @param root
	 *            {@link File} descibing the working directory
	 */
	public MyHTTPHandler1_0(File root) {
		super(root);
		version = new String("HTTP/1.0");

	}

	/**
	 * Constructor using fields
	 * 
	 * @param root
	 *            {@link File} descibing the working directory
	 * @param host
	 *            {@link String} describing the specific host the handler must serve
	 */
	public MyHTTPHandler1_0(File root, String host) {
		super(root, host);
		version = new String("HTTP/1.0");

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected HTTPReply generateReply(HTTPRequest request) {
		HTTPReply reply = null;
		File requested = new File(getRoot(), request.getPath());
		if (request.getVersion().equals("HTTP/1.1")) {
			reply = wrongVersion();
		} else if (!requested.exists()) {
			reply = notFound();
		} else if (request.getMethod().equals("GET")) {
			reply = get(requested);
		} else if (request.getMethod().equals("POST")) {
			reply = post(requested, request);
		} else if (request.getMethod().equals("HEAD")) {
			reply = head(requested);
		}
		return reply;
	}

	/**
	 * Method generating the reply for a message with the wrong version
	 * 
	 * @param request
	 *            {@link HTTPRequest} received
	 * @return suitable {@link HTTPReply}
	 */
	private HTTPReply wrongVersion() {
		String version = getVersion();
		String statusCode = new String("505");
		String statusMessage = new String("HTTP Version Not Supported");
		ConcurrentHashMap<String, String> parameters = new ConcurrentHashMap<String, String>();
		Date date = new Date();
		parameters.put("Date", date.toString());
		return new MyHTTPReply(version, statusCode, statusMessage, "", parameters);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getVersion() {
		return version;
	}
}
