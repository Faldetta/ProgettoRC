package it.unifi.rc.httpserver.m5781374.handler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import it.unifi.rc.httpserver.HTTPHandler;
import it.unifi.rc.httpserver.HTTPReply;
import it.unifi.rc.httpserver.HTTPRequest;
import it.unifi.rc.httpserver.m5781374.request_reply.MyHTTPReply;

/**
 * Abstract class defining {@link HTTPHandler}
 * 
 * @author bow
 *
 */
public abstract class MyHTTPAbstractHandler implements HTTPHandler {

	private File root;
	private String host;

	/**
	 * Constructor using field
	 * 
	 * @param root
	 *            {@link File} descibing the working directory
	 */
	public MyHTTPAbstractHandler(File root) {
		this.root = root;
		this.host = null;
	}

	/**
	 * Constructor using fields
	 * 
	 * @param root
	 *            {@link File} descibing the working directory
	 * @param host
	 *            {@link String} describing the specific host the handler must serve
	 */
	public MyHTTPAbstractHandler(File root, String host) {
		this.root = root;
		this.host = host;
	}

	/**
	 * Method to generate the right {@link HTTPReply}
	 * 
	 * @param request
	 *            received {@link HTTPRequest}
	 * @return a suitable {@link HTTPReply}
	 */
	protected abstract HTTPReply generateReply(HTTPRequest request);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HTTPReply handle(HTTPRequest request) {
		HTTPReply reply = null;
		if ((host != null && request.getParameters().containsValue(host)) || host == null) {
			reply = generateReply(request);
		}
		return reply;
	}

	/**
	 * Method generating the reply for a message with a wrong path
	 * 
	 * @param request
	 *            {@link HTTPRequest} received
	 * @return suitable {@link HTTPReply}
	 */
	protected HTTPReply notFound() {
		String version = getVersion();
		String statusCode = new String("404");
		String statusMessage = new String("Not Found");
		ConcurrentHashMap<String, String> parameters = new ConcurrentHashMap<String, String>();
		Date date = new Date();
		parameters.put("Date", date.toString());
		return new MyHTTPReply(version, statusCode, statusMessage, "", parameters);
	}

	/**
	 * Method generating the reply for a message with the GET method
	 * 
	 * @param request
	 *            {@link HTTPRequest} received
	 * @return suitable {@link HTTPReply}
	 */
	protected HTTPReply get(File requested) {
		HTTPReply reply;
		String version = getVersion();
		String statusCode = new String("200");
		String statusMessage = new String("OK");
		ConcurrentHashMap<String, String> parameters = new ConcurrentHashMap<String, String>();
		Date date = new Date();
		parameters.put("Date", date.toString());
		date = new Date(requested.lastModified());
		parameters.put("Last-Modified", date.toString());
		parameters.put("Content-Type", "text/html");
		String data;
		try {
			data = readFile(requested);
			parameters.put("Content-Length", String.valueOf(data.length()));
			reply = new MyHTTPReply(version, statusCode, statusMessage, data, parameters);
		} catch (IOException e) {
			reply = interalError(e.getMessage());
		}
		return reply;
	}

	/**
	 * Method generating the reply for a message with the POST method
	 * 
	 * @param exceptionMessage
	 *            {@linkString} scpecifying the interna error {@link HTTPRequest}
	 *            received
	 * @return suitable {@link HTTPReply}
	 */
	protected HTTPReply interalError(String exceptionMessage) {
		String version = getVersion();
		String statusCode = new String("500");
		String statusMessage = new String("Internal Server Error");
		ConcurrentHashMap<String, String> parameters = new ConcurrentHashMap<String, String>();
		Date date = new Date();
		parameters.put("Date", date.toString());
		return new MyHTTPReply(version, statusCode, statusMessage, exceptionMessage, parameters);
	}

	/**
	 * Method generating the reply for a message with the POST method
	 * 
	 * @param request
	 *            {@link HTTPRequest} received
	 * @return suitable {@link HTTPReply}
	 */
	protected HTTPReply post(File requested, HTTPRequest request) {
		HTTPReply reply;
		String version = getVersion();
		String statusCode = new String("200");
		String statusMessage = new String("OK");
		ConcurrentHashMap<String, String> parameters = new ConcurrentHashMap<String, String>();
		Date date = new Date();
		parameters.put("Date", date.toString());
		date = new Date(requested.lastModified());
		parameters.put("Last-Modified", date.toString());
		parameters.put("Content-Type", "text/html");
		String data = new String("RESULT");
		// in mancanza di mototre php scivo su file
		try {
			writeFile(request, requested);
			reply = new MyHTTPReply(version, statusCode, statusMessage, data, parameters);
		} catch (IOException e) {
			reply = interalError(e.getMessage());
		}
		return reply;
	}

	/**
	 * Method generating the reply for a message with the HEAD method
	 * 
	 * @param request
	 *            {@link HTTPRequest} received
	 * @return suitable {@link HTTPReply}
	 */
	protected HTTPReply head(File requested) {
		String version = getVersion();
		String statusCode = new String("200");
		String statusMessage = new String("OK");
		ConcurrentHashMap<String, String> parameters = new ConcurrentHashMap<String, String>();
		Date date = new Date();
		parameters.put("Date", date.toString());
		date = new Date(requested.lastModified());
		parameters.put("Last-Modified", date.toString());
		parameters.put("Content-Type", "text/html");
		return new MyHTTPReply(version, statusCode, statusMessage, "", parameters);
	}

	/**
	 * Method to read a file
	 * 
	 * @param requested
	 *            {@link File} to read
	 * @return readed {@link String}
	 * @throws IOException
	 */
	protected String readFile(File requested) throws IOException {
		String readed = new String();
		BufferedReader reader = new BufferedReader(new FileReader(requested));
		boolean canRead = true;
		int tmpChar = 0;
		while (canRead) {
			tmpChar = reader.read();
			if (tmpChar == -1) {
				canRead = false;
			} else {
				readed = readed.concat(String.valueOf((char) tmpChar));
			}
		}
		reader.close();
		return readed;
	}

	/**
	 * Method used to generate a side effect to PUT and POST method
	 * 
	 * @param request
	 *            {@link HTTPRequest} to take the body to write on the file
	 * @param requested
	 *            {@link File} to write into
	 * @throws IOException
	 */
	protected void writeFile(HTTPRequest request, File requested) throws IOException {
		char[] toWrite = request.getEntityBody().toCharArray();
		BufferedWriter writer = new BufferedWriter(new FileWriter(requested));
		for (int i = 0; i < toWrite.length; i++) {
			writer.append(toWrite[i]);
		}
		writer.append((char) 13);
		writer.append((char) 10);
		writer.close();
	}

	/**
	 * Root getter
	 * 
	 * @return {@link File} root
	 */
	protected File getRoot() {
		return root;
	}

	/**
	 * Host getter
	 * 
	 * @return {@link String} host
	 */
	protected String getHost() {
		return host;
	}

	/**
	 * Abstract version getter
	 * 
	 * @return the version's {@link String}
	 */
	protected abstract String getVersion();

}