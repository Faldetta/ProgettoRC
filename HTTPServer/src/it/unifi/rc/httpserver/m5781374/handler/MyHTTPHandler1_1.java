package it.unifi.rc.httpserver.m5781374.handler;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import it.unifi.rc.httpserver.HTTPHandler;
import it.unifi.rc.httpserver.HTTPReply;
import it.unifi.rc.httpserver.HTTPRequest;
import it.unifi.rc.httpserver.m5781374.request_reply.MyHTTPReply;

/**
 * Class defining a 1.1 {@link HTTPHandler}
 * 
 * @author bow
 *
 */
public class MyHTTPHandler1_1 extends MyHTTPAbstractHandler implements HTTPHandler {

	private String version;

	/**
	 * Constructor using field
	 * 
	 * @param root
	 *            {@link File} descibing the working directory
	 */
	public MyHTTPHandler1_1(File root) {
		super(root);
		version = new String("HTTP/1.1");
	}

	/**
	 * Constructor using fields
	 * 
	 * @param root
	 *            {@link File} descibing the working directory
	 * @param host
	 *            {@link String} describing the specific host the handler must serve
	 */
	public MyHTTPHandler1_1(File root, String host) {
		super(root, host);
		version = new String("HTTP/1.1");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected HTTPReply generateReply(HTTPRequest request) {
		HTTPReply reply = null;
		File requested = new File(getRoot(), request.getPath());
		if (request.getVersion().equals("HTTP/1.0")) {
			reply = wrongVersion(request);
		} else if (!requested.exists() && !request.getMethod().equals("PUT")) {
			reply = notFound();
		} else if (request.getMethod().equals("GET")) {
			reply = get(requested);
		} else if (request.getMethod().equals("POST")) {
			reply = post(requested, request);
		} else if (request.getMethod().equals("HEAD")) {
			reply = head(requested);
		} else if (request.getMethod().equals("PUT")) {
			reply = put(requested, request);
		} else if (request.getMethod().equals("DELETE")) {
			reply = delete(requested);
		}
		return reply;
	}

	/**
	 * Method generating the reply for a message with the wrong version
	 * 
	 * @param request
	 *            {@link HTTPRequest} received
	 * @return suitable {@link HTTPReply}
	 */
	private HTTPReply wrongVersion(HTTPRequest request) {
		HTTPReply reply = null;
		MyHTTPHandler1_0 handler1_0 = null;
		if (getHost() == null) {
			handler1_0 = new MyHTTPHandler1_0(getRoot());
		} else {
			handler1_0 = new MyHTTPHandler1_0(getRoot(), getHost());
		}
		reply = handler1_0.handle(request);
		return reply;
	}

	/**
	 * Method generating the reply for a message with the PUT method
	 * 
	 * @param request
	 *            {@link HTTPRequest} received
	 * @return suitable {@link HTTPReply}
	 */
	private HTTPReply put(File requested, HTTPRequest request) {
		HTTPReply reply;
		String version = getVersion();
		String statusCode = new String("204");
		String statusMessage = new String("No Content");
		ConcurrentHashMap<String, String> parameters = new ConcurrentHashMap<String, String>();
		Date date = new Date();
		parameters.put("Date", date.toString());
		date = new Date(requested.lastModified());
		parameters.put("Last-Modified", date.toString());
		try {
			requested.createNewFile();
			writeFile(request, requested);
			reply = new MyHTTPReply(version, statusCode, statusMessage, "", parameters);
		} catch (IOException e) {
			reply = interalError(e.getMessage());
		}
		return reply;
	}

	/**
	 * Method generating the reply for a message with the DELETE method
	 * 
	 * @param request
	 *            {@link HTTPRequest} received
	 * @return suitable {@link HTTPReply}
	 */
	private HTTPReply delete(File requested) {
		String version = getVersion();
		String statusCode = new String("204");
		String statusMessage = new String("No Content");
		ConcurrentHashMap<String, String> parameters = new ConcurrentHashMap<String, String>();
		Date date = new Date();
		parameters.put("Date", date.toString());
		date = new Date(requested.lastModified());
		parameters.put("Last-Modified", date.toString());
		requested.delete();
		return new MyHTTPReply(version, statusCode, statusMessage, "", parameters);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getVersion() {
		return version;
	}

}
