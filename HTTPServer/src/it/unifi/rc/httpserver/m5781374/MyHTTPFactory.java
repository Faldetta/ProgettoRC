package it.unifi.rc.httpserver.m5781374;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;

import it.unifi.rc.httpserver.HTTPFactory;
import it.unifi.rc.httpserver.HTTPHandler;
import it.unifi.rc.httpserver.HTTPInputStream;
import it.unifi.rc.httpserver.HTTPOutputStream;
import it.unifi.rc.httpserver.HTTPServer;
import it.unifi.rc.httpserver.m5781374.handler.MyHTTPHandler1_0;
import it.unifi.rc.httpserver.m5781374.handler.MyHTTPHandler1_1;
import it.unifi.rc.httpserver.m5781374.streams.MyHTTPInputStream;
import it.unifi.rc.httpserver.m5781374.streams.MyHTTPOutputStream;

/**
 * Class used to build the HTTP server components
 * 
 * @author bow
 *
 */
public class MyHTTPFactory implements HTTPFactory {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HTTPInputStream getHTTPInputStream(InputStream is) {
		return new MyHTTPInputStream(is);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HTTPOutputStream getHTTPOutputStream(OutputStream os) {
		return new MyHTTPOutputStream(os);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HTTPServer getHTTPServer(int port, int backlog, InetAddress address, HTTPHandler... handlers) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HTTPHandler getFileSystemHandler1_0(File root) {
		return new MyHTTPHandler1_0(root);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HTTPHandler getFileSystemHandler1_0(String host, File root) {
		return new MyHTTPHandler1_0(root, host);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HTTPHandler getFileSystemHandler1_1(File root) {
		return new MyHTTPHandler1_1(root);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HTTPHandler getFileSystemHandler1_1(String host, File root) {
		return new MyHTTPHandler1_1(root, host);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HTTPHandler getProxyHandler() {
		// TODO Auto-generated method stub
		return null;
	}

}
