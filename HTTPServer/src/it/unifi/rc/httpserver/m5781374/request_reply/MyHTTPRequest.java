package it.unifi.rc.httpserver.m5781374.request_reply;

import java.util.Map;

import it.unifi.rc.httpserver.HTTPRequest;

/**
 * Class defining {@link HTTPRequest}.
 * 
 * @author bow
 *
 */
public class MyHTTPRequest implements HTTPRequest {

	private String version;
	private String method;
	private String path;
	private String entityBody;
	private Map<String, String> parameters;

	/**
	 * Constructor with all parammeters
	 * 
	 * @param version
	 *            {@link String} defining HTTP protocol's version
	 * @param method
	 *            {@link String} defining HTTP method used
	 * @param path
	 *            {@link String} defining path requested
	 * @param entityBody
	 *            {@link String} defining the body of the request
	 * @param parameters
	 *            {@link Map} of the header lines of the HTTP request
	 */
	public MyHTTPRequest(String version, String method, String path, String entityBody,
			Map<String, String> parameters) {
		super();
		this.version = version;
		this.method = method;
		this.path = path;
		this.entityBody = entityBody;
		this.parameters = parameters;
	}

	/**
	 * Version getter
	 * 
	 * @return {@link String} containing the version of the protocol of the request
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/**
	 * Method getter
	 * 
	 * @return {@link String} containing the method of the request
	 */
	@Override
	public String getMethod() {
		return method;
	}

	/**
	 * Path getter
	 * 
	 * @return {@link String} containing the requested pathe
	 */
	@Override
	public String getPath() {
		return path;
	}

	/**
	 * Body getter
	 * 
	 * @return {@link String} containing the request's body
	 */
	@Override
	public String getEntityBody() {
		return entityBody;
	}

	/**
	 * Parameters getter
	 * 
	 * @return {@link Map} containing the header lines
	 */
	@Override
	public Map<String, String> getParameters() {
		return parameters;
	}

	/**
	 * Override of the Objact's equals method
	 * 
	 * @param obj
	 *            {@link Object} to be tested
	 * 
	 * @return true if obj is equals to the caller object, false otherwise
	 */
	@Override
	public boolean equals(Object obj) {
		boolean isEqual = false;
		if (obj instanceof MyHTTPRequest) {
			MyHTTPRequest request = (MyHTTPRequest) obj;
			if (version.equals(request.getVersion()) && method.equals(request.getMethod())
					&& path.equals(request.getPath()) && entityBody.equals(request.getEntityBody())
					&& parameters.equals(request.getParameters())) {
				isEqual = true;
			}
		}
		return isEqual;
	}
}
