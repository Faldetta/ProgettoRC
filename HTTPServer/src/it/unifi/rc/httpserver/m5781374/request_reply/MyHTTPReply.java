package it.unifi.rc.httpserver.m5781374.request_reply;

import java.util.Map;

import it.unifi.rc.httpserver.HTTPReply;

/**
 * Class defining {@link HTTPReply}.
 * 
 * @author bow
 *
 */
public class MyHTTPReply implements HTTPReply {

	private String version;
	private String statusCode;
	private String statusMessage;
	private String data;
	private Map<String, String> parameters;

	/**
	 * Constructor with all parammeters
	 * 
	 * @param version
	 *            {@link String} defining HTTP protocol's versionion
	 * @param statusCode
	 *            {@link String} defining HTTP status code
	 * @param statusMessage
	 *            {@link String} defining HTTP status message
	 * @param data
	 *            {@link String} defining HTTP reply's data
	 * @param parameters
	 *            {@link Map} of the header lines of the HTTP reply
	 */
	public MyHTTPReply(String version, String statusCode, String statusMessage, String data,
			Map<String, String> parameters) {
		super();
		this.version = version;
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
		this.data = data;
		this.parameters = parameters;
	}

	/**
	 * Version getter
	 * 
	 * @return {@link String} containing the version of the protocol of the reply
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/**
	 * Status code getter
	 * 
	 * @return {@link String} containing the reply' status code
	 */
	@Override
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * Status message getter
	 * 
	 * @return {@link String} containing the reply' status message
	 */
	@Override
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * Data getter
	 * 
	 * @return {@link String} containing the request's body
	 */
	@Override
	public String getData() {
		return data;
	}

	/**
	 * Parameters getter
	 * 
	 * @return {@link Map} containing the header lines
	 */
	@Override
	public Map<String, String> getParameters() {
		return parameters;
	}

	/**
	 * Override of the Objact's equals method
	 * 
	 * @param obj
	 *            {@link Object} to be tested
	 * 
	 * @return true if obj is equals to the caller object, false otherwise
	 */
	@Override
	public boolean equals(Object obj) {
		boolean isEqual = false;
		if (obj instanceof MyHTTPReply) {
			MyHTTPReply reply = (MyHTTPReply) obj;
			if (version.equals(reply.getVersion()) && statusCode.equals(reply.getStatusCode())
					&& statusMessage.equals(reply.getStatusMessage()) && data.equals(reply.getData())
					&& parameters.equals(reply.getParameters())) {
				isEqual = true;
			}
		}
		return isEqual;
	}
}
