package it.unifi.rc.httpserver.m5781374.streams;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import it.unifi.rc.httpserver.HTTPOutputStream;
import it.unifi.rc.httpserver.HTTPProtocolException;
import it.unifi.rc.httpserver.HTTPReply;
import it.unifi.rc.httpserver.HTTPRequest;

/**
 * Class defining {@link HTTPOutputStream}
 * 
 * @author bow
 * 
 */
public class MyHTTPOutputStream extends HTTPOutputStream {

	/**
	 * Field identifying the {@link BufferedWriter} used
	 */
	private BufferedWriter out;

	public MyHTTPOutputStream(OutputStream os) {
		super(os);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void setOutputStream(OutputStream os) {
		try {
			out = new BufferedWriter(new OutputStreamWriter(os, "US-ASCII"));
		} catch (UnsupportedEncodingException e) {
			// non dovrebbe essere mai eseguito
			e.printStackTrace();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeHttpReply(HTTPReply reply) {
		try {
			String replyString = new String();
			replyString = replyString
					.concat(reply.getVersion() + " " + reply.getStatusCode() + " " + reply.getStatusMessage() + "\r\n"
							+ writeHeader(reply.getParameters()) + "\r\n" + reply.getData());
			out.write(replyString);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to convert the given {@link Map} into a {@link String} with the header
	 * lines
	 * 
	 * @param parameters
	 *            {@link Map} with the header lines
	 * @return {@link String} with the correct HTTP header lines format
	 */
	private String writeHeader(Map<String, String> parameters) {
		Set<Map.Entry<String, String>> entrySet = parameters.entrySet();
		Iterator<Map.Entry<String, String>> entryIterator = entrySet.iterator();
		String parametersString = new String();
		Entry<String, String> entry;
		while (entryIterator.hasNext()) {
			entry = entryIterator.next();
			parametersString = parametersString.concat(entry.getKey() + ": " + entry.getValue() + "\r\n");
			entryIterator.remove();
		}
		return parametersString;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writeHttpRequest(HTTPRequest request) {
		try {
			String requestString = new String();
			requestString = requestString
					.concat(request.getMethod() + " " + request.getPath() + " " + request.getVersion() + "\r\n"
							+ writeHeader(request.getParameters()) + "\r\n" + request.getEntityBody());
			out.write(requestString);
		} catch (IOException e) {
			e = new HTTPProtocolException("errore scrittura stream");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() {
		try {
			out.close();
		} catch (IOException e) {
			e = new HTTPProtocolException("errore chiusura output stream");
		}
	}

}
