package it.unifi.rc.httpserver.m5781374.streams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import it.unifi.rc.httpserver.HTTPInputStream;
import it.unifi.rc.httpserver.HTTPProtocolException;
import it.unifi.rc.httpserver.HTTPReply;
import it.unifi.rc.httpserver.HTTPRequest;
import it.unifi.rc.httpserver.m5781374.request_reply.MyHTTPReply;
import it.unifi.rc.httpserver.m5781374.request_reply.MyHTTPRequest;

/**
 * Class defining {@link HTTPInputStream}
 * 
 * @author bow
 * 
 */
public class MyHTTPInputStream extends HTTPInputStream {

	/**
	 * Field identifying the {@link BufferedReader} used
	 */
	private BufferedReader in;

	/**
	 * Super constructor using field
	 * 
	 * @param is
	 *            {@link InputStream} to use
	 */
	public MyHTTPInputStream(InputStream is) {
		super(is);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void setInputStream(InputStream is) {
		try {
			in = new BufferedReader(new InputStreamReader(is, "US-ASCII"));
		} catch (UnsupportedEncodingException e) {
			// non dovrei mai arrivare all'eccezione
			e.printStackTrace();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HTTPRequest readHttpRequest() throws HTTPProtocolException {
		String version, method, path, entityBody;
		Map<String, String> parameters;
		method = readUntilChar(in, 32); // 32 == space
		path = readUntilChar(in, 32); // 32 == space
		version = readUntilChar(in, 13); // 13 == CR
		readUntilChar(in, 10); // 10 == LF
		parameters = readParameters(in);
		entityBody = readUntilChar(in, -1); // -1 == end of stream
		return new MyHTTPRequest(version, method, path, entityBody, parameters);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HTTPReply readHttpReply() throws HTTPProtocolException {
		String version, statusCode, statusMessage, data;
		Map<String, String> parameters;
		version = readUntilChar(in, 32); // 32 == space
		statusCode = readUntilChar(in, 32); // 32 == space
		statusMessage = readUntilChar(in, 13); // 13 == CR
		readUntilChar(in, 10); // 10 == LF
		parameters = readParameters(in);
		data = readUntilChar(in, -1); // -1 == end of stream
		return new MyHTTPReply(version, statusCode, statusMessage, data, parameters);
	}

	/**
	 * 
	 * @param in
	 *            {@link BufferedReader} to read from
	 * @param lastChar
	 *            the <code>int</code> ascii value of the last char (not to be
	 *            readed)
	 * @return readed {@link String} if the first character is not equals to CR,
	 *         null otherwise
	 */
	private String readUntilChar(BufferedReader in, int lastChar) {
		String readed = new String();
		boolean endingCharFlag = false;
		int tmpChar = 0;
		int index = 0;
		while (!endingCharFlag) {
			try {
				tmpChar = in.read();
				if (index == 0 && tmpChar == 13) { // 13 == CR
					endingCharFlag = true;
					readed = null;
				} else if (tmpChar == lastChar || tmpChar == -1) {
					endingCharFlag = true;
				} else {
					readed = readed.concat(String.valueOf((char) tmpChar));
				}
				index++;
			} catch (IOException e) {
				e = new HTTPProtocolException("errore lettura stream");
			}
		}
		return readed;
	}

	/**
	 * Method to convert the readed header lines into a {@link Map}
	 * 
	 * @param in
	 *            {@link BufferedReader} to read the HTTP header lines (correctly
	 *            formatted)
	 * @return the {@link Map} generated
	 */
	private Map<String, String> readParameters(BufferedReader in) {
		ConcurrentHashMap<String, String> map = new ConcurrentHashMap<String, String>();
		boolean headerEnd = false;
		String variable, value;
		while (!headerEnd) {
			variable = readUntilChar(in, 58); // 58 == :
			if (variable == null) {
				readUntilChar(in, 10);
				headerEnd = true;
			} else {
				readUntilChar(in, 32); // 32 == space
				value = readUntilChar(in, 13); // 13 == CR
				readUntilChar(in, 10); // 10 == LF
				if (value == null) {
					headerEnd = true;
				} else {
					map.put(variable, value);
				}
			}
		}
		return map;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close()  {
		try {
			in.close();
		} catch (IOException e) {
			e = new HTTPProtocolException("errore chiusura input stream");
		}
	}
}
